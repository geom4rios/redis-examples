package com.geom4rios.redisExamples.cluster;

import com.geom4rios.redisExamples.common.RedisService;
import io.lettuce.core.ReadFrom;
import io.lettuce.core.RedisURI;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import io.lettuce.core.cluster.api.sync.RedisAdvancedClusterCommands;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log4j2
public class RedisClusterService implements RedisService {

    private static volatile RedisClusterService redisClusterService;
    private final RedisClusterProperties redisClusterProperties;
    List<RedisURI> nodes = new ArrayList<>();

    private RedisClusterService() {
        this.redisClusterProperties = RedisClusterProperties.getInstance();
    }

    public static RedisClusterService getInstance() {
        if (redisClusterService == null) {
            synchronized (RedisClusterService.class) {
                if (redisClusterService == null) {
                    redisClusterService = new RedisClusterService();
                }
            }
        }
        return redisClusterService;
    }

    private void init() {
        log.info("Redis service init");
        nodes = Arrays.asList(
                RedisURI.Builder
                        .redis(redisClusterProperties.getRedisClusterNode1Host(), redisClusterProperties.getRedisClusterNode1Port())
                        .withPassword(redisClusterProperties.getRedisClusterPassword().toCharArray())
                        .build(),
                RedisURI.Builder
                        .redis(redisClusterProperties.getRedisClusterNode2Host(), redisClusterProperties.getRedisClusterNode2Port())
                        .withPassword(redisClusterProperties.getRedisClusterPassword().toCharArray())
                        .build(),
                RedisURI.Builder
                        .redis(redisClusterProperties.getRedisClusterNode3Host(), redisClusterProperties.getRedisClusterNode3Port())
                        .withPassword(redisClusterProperties.getRedisClusterPassword().toCharArray())
                        .build()
        );
    }

    @Override
    public void saveKeyValue(String key, String value) {
        RedisClusterClient clusterClient = null;
        StatefulRedisClusterConnection<String, String> connection = null;
        try {
            clusterClient = RedisClusterClient.create(nodes);
            connection = clusterClient.connect();
            connection.setReadFrom(ReadFrom.ANY_REPLICA);
            RedisAdvancedClusterCommands<String, String> syncCommands =  connection.sync();
            syncCommands.set(key, value);
        } catch (Exception e) {
            log.error("Error while saving key value", e);
            log.error(e.getMessage(), e);
        } finally{
            if (clusterClient != null) {
                clusterClient.shutdown();
            }
            if (connection != null) {
                connection.close();
            }
        }

    }
}
