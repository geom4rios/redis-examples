package com.geom4rios.redisExamples.cluster;

import com.geom4rios.redisExamples.common.PropertiesLoader;
import com.geom4rios.redisExamples.masterReplica.RedisMasterReplicaProperties;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;

@Log4j2
@Getter
public class RedisClusterProperties extends PropertiesLoader {

    private static volatile RedisClusterProperties redisProperties;

    private RedisClusterProperties() {
        init();
    }

    public static RedisClusterProperties getInstance() {
        if (redisProperties == null) {
            synchronized (RedisClusterProperties.class) {
                if (redisProperties == null) {
                    redisProperties = new RedisClusterProperties();
                }
            }
        }
        return redisProperties;
    }

    private void init() {
        try {
            Map<String, String> redisProperties = new HashMap<>();
            loadProperties(redisProperties, "redis-cluster", "REDIS CLUSTER PROPERTIES");
            this.redisClusterNode1Host = redisProperties.get("redis.cluster.node1.host");
            this.redisClusterNode1Port = Integer.parseInt(redisProperties.get("redis.cluster.node1.port"));
            this.redisClusterNode2Host = redisProperties.get("redis.cluster.node2.host");
            this.redisClusterNode2Port = Integer.parseInt(redisProperties.get("redis.cluster.node2.port"));
            this.redisClusterNode3Host = redisProperties.get("redis.cluster.node3.host");
            this.redisClusterNode3Port = Integer.parseInt(redisProperties.get("redis.cluster.node3.port"));
            this.redisClusterUsername = redisProperties.get("redis.cluster.username");
            this.redisClusterPassword = redisProperties.get("redis.cluster.password");
        } catch (Exception e) {
            log.error("Unable to initialize " + this.getClass().getName() + " correctly!");
            log.error(e.getMessage(), e);
        }
    }

    private String redisClusterNode1Host;
    private Integer redisClusterNode1Port;
    private String redisClusterNode2Host;
    private Integer redisClusterNode2Port;
    private String redisClusterNode3Host;
    private Integer redisClusterNode3Port;
    private String redisClusterUsername;
    private String redisClusterPassword;;
}
