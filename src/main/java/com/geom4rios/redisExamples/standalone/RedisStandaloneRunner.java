package com.geom4rios.redisExamples.standalone;

public class RedisStandaloneRunner {

    public static void main(String[] args) {
        RedisStandaloneService redisStandaloneService = RedisStandaloneService.getInstance();
        redisStandaloneService.saveKeyValue("standalone key1", "standalone value1");
    }

}
