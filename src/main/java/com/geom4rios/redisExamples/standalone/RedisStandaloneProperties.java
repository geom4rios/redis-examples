package com.geom4rios.redisExamples.standalone;

import com.geom4rios.redisExamples.common.PropertiesLoader;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;

@Log4j2
@Getter
public class RedisStandaloneProperties extends PropertiesLoader {

    private static volatile RedisStandaloneProperties redisProperties;

    private RedisStandaloneProperties() {
        init();
    }

    public static RedisStandaloneProperties getInstance() {
        if (redisProperties == null) {
            synchronized (RedisStandaloneProperties.class) {
                if (redisProperties == null) {
                    redisProperties = new RedisStandaloneProperties();
                }
            }
        }
        return redisProperties;
    }

    private void init() {
        try {
            Map<String, String> redisProperties = new HashMap<>();
            loadProperties(redisProperties, "redis-standalone", "REDIS STANDALONE PROPERTIES");
            this.host = redisProperties.getOrDefault("redis.standalone.host", null);
            String portStr = redisProperties.getOrDefault("redis.standalone.port", null);
            this.port = Integer.parseInt(portStr);
            this.password = redisProperties.getOrDefault("redis.standalone.password", null);
            this.database = Integer.parseInt(redisProperties.getOrDefault("redis.standalone.database", "0"));
        } catch (Exception e) {
            log.error("Unable to initialize " + this.getClass().getName() + " correctly!");
            log.error(e.getMessage(), e);
        }
    }

    private String host;
    private int port;
    private String password;
    private int database;
}
