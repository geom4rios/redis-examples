package com.geom4rios.redisExamples.standalone;

import com.geom4rios.redisExamples.common.RedisService;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class RedisStandaloneService implements RedisService {

    private static volatile RedisStandaloneService redisStandaloneService;
    private final RedisStandaloneProperties redisStandaloneProperties;

    private RedisStandaloneService() {
        this.redisStandaloneProperties = RedisStandaloneProperties.getInstance();
    }

    public static RedisStandaloneService getInstance() {
        if (redisStandaloneService == null) {
            synchronized (RedisStandaloneService.class) {
                if (redisStandaloneService == null) {
                    redisStandaloneService = new RedisStandaloneService();
                }
            }
        }
        return redisStandaloneService;
    }

    @Override
    public void saveKeyValue(String key, String value) {
        RedisClient client = null;
        StatefulRedisConnection<String, String> connection = null;
        try {
            log.info("Saving key-value pair: {} - {}", key, value);
            RedisURI redisUri = RedisURI.Builder.redis(redisStandaloneProperties.getHost(), redisStandaloneProperties.getPort())
                    .withPassword(redisStandaloneProperties.getPassword().toCharArray())
                    .withDatabase(redisStandaloneProperties.getDatabase())
                    .build();
            client = RedisClient.create(redisUri);
            connection = client.connect();
            RedisCommands<String, String> commands = connection.sync();
            commands.set(key, value);
            connection.close();
        } catch (Exception e) {
            log.error("Error saving key-value pair: {} - {}", key, value, e);
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (client != null) {
                client.shutdown();
            }
        }
    }

}
