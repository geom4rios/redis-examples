package com.geom4rios.redisExamples.common;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

@Log4j2
public abstract class PropertiesLoader {

    protected void loadProperties(Map<String, String> map, String fileName, String message) {
        ResourceBundle rb = ResourceBundle.getBundle(fileName);
        Set<String> keys = rb.keySet();
        keys.forEach(key -> map.put(key, rb.getString(key)));
        convertEnvVariablePropertiesToValues(map, message);
    }

    protected void convertEnvVariablePropertiesToValues(Map<String, String> map, String message) {
        for (Map.Entry<String, String> mapPropertyEntry : map.entrySet()) {
            String propValue = mapPropertyEntry.getValue();
            if (mapPropertyEntry.getValue().startsWith("${") && mapPropertyEntry.getValue().endsWith("}")) {
                String envVariableName = StringUtils.substringBetween(propValue, "${", "}");
                map.replace(mapPropertyEntry.getKey(), System.getenv(envVariableName));
            }
        }
        logPropertiesMap(map, message + ", with resolved env variables.");
    }

    protected void logPropertiesMap(Map<String, String> map, String message) {
        log.info("######### " + message + " #########");
        log.info(map.toString());
        log.info("###################################");
    }

}
