package com.geom4rios.redisExamples.common;

public interface RedisService {

    void saveKeyValue(String key, String value);

}
