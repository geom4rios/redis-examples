package com.geom4rios.redisExamples.masterReplica;

import com.geom4rios.redisExamples.common.PropertiesLoader;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;

@Log4j2
@Getter
public class RedisMasterReplicaProperties extends PropertiesLoader {

    private static volatile RedisMasterReplicaProperties redisProperties;

    private RedisMasterReplicaProperties() {
        init();
    }

    public static RedisMasterReplicaProperties getInstance() {
        if (redisProperties == null) {
            synchronized (RedisMasterReplicaProperties.class) {
                if (redisProperties == null) {
                    redisProperties = new RedisMasterReplicaProperties();
                }
            }
        }
        return redisProperties;
    }

    private void init() {
        try {
            Map<String, String> redisProperties = new HashMap<>();
            loadProperties(redisProperties, "redis-master-replica", "REDIS MASTER REPLICA PROPERTIES");
            this.redisMaster = redisProperties.get("redis.master");
            this.redisMasterPort = redisProperties.get("redis.master.port");
            this.redisReplica1Host = redisProperties.get("redis.replica1.host");
            this.redisReplica1Port = redisProperties.get("redis.replica1.port");
            this.redisReplica2Host = redisProperties.get("redis.replica2.host");
            this.redisReplica2Port = redisProperties.get("redis.replica2.port");
            this.username = redisProperties.get("redis.username");
            this.password = redisProperties.get("redis.password");
        } catch (Exception e) {
            log.error("Unable to initialize " + this.getClass().getName() + " correctly!");
            log.error(e.getMessage(), e);
        }
    }

    private String redisMaster;
    private String redisMasterPort;
    private String redisReplica1Host;
    private String redisReplica1Port;
    private String redisReplica2Host;
    private String redisReplica2Port;
    private String username;
    private String password;
}
