package com.geom4rios.redisExamples.masterReplica;

import com.geom4rios.redisExamples.standalone.RedisStandaloneService;

public class RedisMasterReplicaRunner {

    public static void main(String[] args) {
        RedisMasterReplicaService redisMasterReplicaService = RedisMasterReplicaService.getInstance();
        redisMasterReplicaService.saveKeyValue("master replica key", "master replica value");
    }

}
