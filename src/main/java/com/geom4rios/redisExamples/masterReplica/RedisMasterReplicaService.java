package com.geom4rios.redisExamples.masterReplica;

import com.geom4rios.redisExamples.common.RedisService;
import com.geom4rios.redisExamples.standalone.RedisStandaloneProperties;
import io.lettuce.core.ReadFrom;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.codec.StringCodec;
import io.lettuce.core.masterreplica.MasterReplica;
import io.lettuce.core.masterreplica.StatefulRedisMasterReplicaConnection;
import lombok.extern.log4j.Log4j2;

import java.util.Arrays;
import java.util.List;

@Log4j2
public class RedisMasterReplicaService implements RedisService {

    private static volatile RedisMasterReplicaService redisMasterReplicaService;
    private final RedisMasterReplicaProperties redisMasterReplicaProperties;

    private RedisMasterReplicaService() {
        this.redisMasterReplicaProperties = RedisMasterReplicaProperties.getInstance();
    }

    public static RedisMasterReplicaService getInstance() {
        if (redisMasterReplicaService == null) {
            synchronized (RedisMasterReplicaService.class) {
                if (redisMasterReplicaService == null) {
                    redisMasterReplicaService = new RedisMasterReplicaService();
                }
            }
        }
        return redisMasterReplicaService;
    }

    @Override
    public void saveKeyValue(String key, String value) {
        RedisClient client = RedisClient.create();
        StatefulRedisMasterReplicaConnection<String, String> connection = null;

        List<RedisURI> nodes = Arrays.asList(
                RedisURI.Builder
                        .redis(redisMasterReplicaProperties.getRedisMaster(), Integer.parseInt(redisMasterReplicaProperties.getRedisMasterPort()))
                        .withPassword(redisMasterReplicaProperties.getPassword().toCharArray())
                        .build(),
                RedisURI.Builder
                        .redis(redisMasterReplicaProperties.getRedisReplica1Host(), Integer.parseInt(redisMasterReplicaProperties.getRedisReplica1Port()))
                        .withPassword(redisMasterReplicaProperties.getPassword().toCharArray())
                        .build(),
                RedisURI.Builder
                        .redis(redisMasterReplicaProperties.getRedisReplica2Host(), Integer.parseInt(redisMasterReplicaProperties.getRedisReplica2Port()))
                        .withPassword(redisMasterReplicaProperties.getPassword().toCharArray())
                        .build()
        );

        try {
            log.info("Saving key-value pair: {} - {}", key, value);
            connection = MasterReplica.connect(client, StringCodec.UTF8, nodes);
            connection.setReadFrom(ReadFrom.ANY_REPLICA);
            RedisCommands<String, String> commands = connection.sync();
            commands.set(key, value);
            connection.close();
        } catch (Exception e) {
            log.error("Error saving key-value pair: {} - {}", key, value, e);
            log.error(e.getMessage(), e);
        } finally {
            if (connection != null) {
                connection.close();
            }
            client.shutdown();
        }

    }
}
